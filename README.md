# OpenML dataset: USPS

https://www.openml.org/d/41964

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Binarized version of the USPS dataset (see version 2). Only instances with class labels 6 and 9 from the original dataset are considered and encoded as 0 (original class 6) and 1 (original class 9).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41964) of an [OpenML dataset](https://www.openml.org/d/41964). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41964/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41964/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41964/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

